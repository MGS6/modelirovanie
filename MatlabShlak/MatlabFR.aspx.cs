﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.Arrays;
using LibWeb;



namespace MatlabShlak
{
    public partial class MatlabFR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               
        }

        double polyta, polytb, ab, adb, adbc, vz1350, vz1400, vz1450, vz1500, vz1550, t25p, t7p, grd25, grd1400, a1, a2, a3, b1, b2, b3, xy;

      

        public static string txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12;

        protected void Button1_Click(object sender, EventArgs e)
        {

            Directory.CreateDirectory(Path.GetDirectoryName(@"C:\FR\datematlab.xml"));
            XmlTextWriter writer = new XmlTextWriter(@"C:\FR\datematlab.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("list");
            createNode("a", TextBoxA.Text, writer);
            createNode("b", TextBoxB.Text, writer);
            createNode("c", TextBoxD.Text, writer);
            createNode("d", TextBoxC.Text, writer);
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            ClassLib obj_class = new ClassLib();
            MWArray[] resultab = obj_class.test(1, ab);
            MWArray[] resultab2 = obj_class.test2(1, adb);
            MWArray[] resultab3 = obj_class.test2b(1, adbc);
            MWArray[] resultab4 = obj_class.test7(1, vz1350);
            MWArray[] resultab5 = obj_class.test9(1, vz1400);
            MWArray[] resultab6 = obj_class.test10(1, vz1450);
            MWArray[] resultab7 = obj_class.test11(1, vz1500);
            MWArray[] resultab8 = obj_class.test11b(1, vz1550);
            MWArray[] resultab9 = obj_class.test12(1, t25p);
            MWArray[] resultab10 = obj_class.test13(1, t7p);
            MWArray[] resultab11 = obj_class.test14(1, grd25);
            MWArray[] resultab12 = obj_class.test15(1, grd1400);

            for (int p = 0; p < resultab.Length; p++)
            {
                descriptor = (MWNumericArray)resultab[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt1 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab2.Length; p++)
            {
                descriptor = (MWNumericArray)resultab2[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt2 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab3.Length; p++)
            {
                descriptor = (MWNumericArray)resultab3[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt3 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab4.Length; p++)
            {
                descriptor = (MWNumericArray)resultab4[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt4 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab5.Length; p++)
            {
                descriptor = (MWNumericArray)resultab5[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt5 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab6.Length; p++)
            {
                descriptor = (MWNumericArray)resultab6[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt6 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab7.Length; p++)
            {
                descriptor = (MWNumericArray)resultab7[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt7 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab8.Length; p++)
            {
                descriptor = (MWNumericArray)resultab8[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt8 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab9.Length; p++)
            {
                descriptor = (MWNumericArray)resultab9[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt9 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab10.Length; p++)
            {
                descriptor = (MWNumericArray)resultab10[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt10 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab11.Length; p++)
            {
                descriptor = (MWNumericArray)resultab11[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt11 = (d_descriptor[i, 0].ToString());
                }
            }

            for (int p = 0; p < resultab12.Length; p++)
            {
                descriptor = (MWNumericArray)resultab12[p];
                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);
                for (int i = 0; i < d_descriptor.Length; i++)
                {
                    txt12 = (d_descriptor[i, 0].ToString());
                }
            }

            XmlTextWriter writernew = new XmlTextWriter(@"C:\FR\finalresult.xml", System.Text.Encoding.UTF8);
            writernew.WriteStartDocument(true);
            writernew.Formatting = Formatting.Indented;
            writernew.Indentation = 2;
            writernew.WriteStartElement("list");
            createNodeNew("a/b", txt1, writernew);
            createNodeNew("(a+d)/b", txt2, writernew);
            createNodeNew("(a+d)/(b+c)", txt3, writernew);
            createNodeNew("vz1350", txt4, writernew);
            createNodeNew("vz1400", txt5, writernew);
            createNodeNew("vz1450", txt6, writernew);
            createNodeNew("vz1500", txt7, writernew);
            createNodeNew("vz1550", txt8, writernew);
            createNodeNew("t25p", txt9, writernew);
            createNodeNew("t7p", txt10, writernew);
            createNodeNew("grd25", txt11, writernew);
            createNodeNew("grd1400", txt12, writernew);
            writernew.WriteEndElement();
            writernew.WriteEndDocument();
            writernew.Close();

            Response.Redirect("MatlabFR2.aspx");
        }

        string testread, root;
        string a, b, c, d;


        MWArray[] result = null;
        MWNumericArray descriptor = null;

        private const string XML_FILE_NAME = "datematlab.xml";
        public static XmlDocument LoadSampleXML()
        {
            XmlDocument doc = null;
            try
            {

                doc = new XmlDocument();
                doc.Load(XML_FILE_NAME);
                return doc;
            }
            catch (Exception ex)
            {
                return doc;
            }
        }

        private void createNode(string vname, string vvalue, XmlTextWriter writer)
        {
            writer.WriteStartElement("listitem");
            writer.WriteStartElement("varname");
            writer.WriteString(vname);
            writer.WriteEndElement();
            writer.WriteStartElement("varvalue");
            writer.WriteString(vvalue);
            writer.WriteEndElement();
            writer.WriteEndElement();

        }

        private void createNodeNew(string vname, string vvalue, XmlTextWriter writernew)
        {
            writernew.WriteStartElement("listitem");
            writernew.WriteStartElement("varname");
            writernew.WriteString(vname);
            writernew.WriteEndElement();
            writernew.WriteStartElement("varvalue");
            writernew.WriteString(vvalue);
            writernew.WriteEndElement();
            writernew.WriteEndElement();

        }

    }

}