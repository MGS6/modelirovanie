﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MathWorks.MATLAB.NET.Utility;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using MathWorks.MATLAB.NET.Arrays;
using LibWeb;

namespace MatlabShlak
{
    public partial class MatlabFR2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBox1.Text = MatlabFR.txt1;
            TextBox2.Text = MatlabFR.txt2;
            TextBox3.Text = MatlabFR.txt3;
            TextBox4.Text = MatlabFR.txt4;
            TextBox5.Text = MatlabFR.txt5;
            TextBox6.Text = MatlabFR.txt6;
            TextBox7.Text = MatlabFR.txt7;
            TextBox8.Text = MatlabFR.txt8;
            TextBox9.Text = MatlabFR.txt9;
            TextBox10.Text = MatlabFR.txt10;
            TextBox11.Text = MatlabFR.txt11;
            TextBox12.Text = MatlabFR.txt12;
        }
    }
}