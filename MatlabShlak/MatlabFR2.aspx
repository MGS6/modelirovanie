﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MatlabFR2.aspx.cs" Inherits="MatlabShlak.MatlabFR2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-size: large; font-weight: bold">
    
        Вывод полученных результатов</div>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CaO/SiO2:&nbsp;&nbsp;
        <asp:TextBox ID="TextBox1" runat="server" Width="223px"></asp:TextBox>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (CaO+MgO)/SiO2:&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (CaO+MgO)/(SiO2+Al2O3):&nbsp;&nbsp;
            <asp:TextBox ID="TextBox3" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Вязкость при т-ре 1350:&nbsp;
            <asp:TextBox ID="TextBox4" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Вязкость при т-ре 1400:&nbsp;
            <asp:TextBox ID="TextBox5" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Вязкость при т-ре 1450:&nbsp;
            <asp:TextBox ID="TextBox6" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Вязкость при т-ре 1500:&nbsp;&nbsp;
            <asp:TextBox ID="TextBox7" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Вязкость при т-ре 1550:&nbsp;
            <asp:TextBox ID="TextBox8" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Тем-ра при вяз-сти 25 пуаз:&nbsp;&nbsp;
            <asp:TextBox ID="TextBox9" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Тем-ра при вяз-сти 7 пуаз:&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox10" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Градиент вяз-сти 25-7 пуаз:&nbsp;&nbsp;
            <asp:TextBox ID="TextBox11" runat="server" Width="223px"></asp:TextBox>
        </p>
        <p>
&nbsp;Градиент вяз-сти 1400-1500 С:&nbsp;&nbsp;
            <asp:TextBox ID="TextBox12" runat="server" Width="223px"></asp:TextBox>
        </p>
    </form>
</body>
</html>
