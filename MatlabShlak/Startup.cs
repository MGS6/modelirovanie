﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MatlabShlak.Startup))]
namespace MatlabShlak
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
